package com.telegrambot.bot.repository.entity.enums;

public enum Role {
    ADMIN,
    USER;
}
