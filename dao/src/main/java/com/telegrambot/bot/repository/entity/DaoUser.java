package com.telegrambot.bot.repository.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "pa_user")
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class DaoUser extends BaseEntity {

    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "telegram_id", nullable = false, unique = true)
    private Integer telegramId;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "chat_id", nullable = false, unique = true)
    private Long chatId;

    @OneToMany(mappedBy = "user")
    @Column(name = "roles")
    private Set<UserRole> roles = new HashSet<>();

    public void addRole(UserRole userRole) {
        roles.add(userRole);
    }
}
