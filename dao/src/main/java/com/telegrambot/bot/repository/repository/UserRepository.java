package com.telegrambot.bot.repository.repository;

import com.telegrambot.bot.repository.entity.DaoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<DaoUser, Long> {
    Optional<DaoUser> findByTelegramId(Integer integer);
    Optional<DaoUser> findByFirstName(String firstName);
}
