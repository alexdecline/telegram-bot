create table pa_user_roles
(
	id bigserial,
	user_role varchar not null,
	created_dt  timestamp,
	user_id bigint
		constraint pa_user_roles_pa_user_id_fk
			references pa_user
);

create unique index pa_user_roles_id_uindex
	on pa_user_roles (id);

alter table pa_user_roles
	add constraint pa_user_roles_pk
		primary key (id);

