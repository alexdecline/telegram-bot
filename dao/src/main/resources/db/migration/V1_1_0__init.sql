create table pa_user
(
    id          bigserial   not null,
    created_dt  timestamp,
    first_name  varchar(50),
    last_name   varchar(50),
    middle_name varchar(50),
    telegram_id int4        not null,
    user_name   varchar(50) not null,
    primary key (id)
);

alter table pa_user
    add constraint UK_1c250up810pf0d1l10wb974vy unique (telegram_id)