alter table pa_user
	add chat_id int8;

create unique index pa_user_chat_id_uindex
	on pa_user (chat_id);