package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.container.HandlerBotMethodParameters;
import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import org.telegram.telegrambots.meta.api.objects.Update;

public class MethodArgumentHandler extends AbstractSingleHandler {

    private final HandlerBotMethodParameters handlerBotMethodParameters;

    public MethodArgumentHandler(HandlerBotMethodParameters handlerBotMethodParameters) {
        this.handlerBotMethodParameters = handlerBotMethodParameters;
    }

    public MethodArgumentHandler(SingleHandler nextHandler,
                                 HandlerBotMethodParameters handlerBotMethodParameters) {
        super(nextHandler);
        this.handlerBotMethodParameters = handlerBotMethodParameters;
    }

    @Override
    public void handle(Update update, BotRequestResponseInfoProvider info) {
        handlerBotMethodParameters.resolveParametersAndGetArguments(info, info.getMessage());

        startNextHandler(update, info);
    }
}
