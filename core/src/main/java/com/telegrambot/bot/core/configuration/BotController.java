package com.telegrambot.bot.core.configuration;

import org.springframework.stereotype.Service;

import java.lang.annotation.*;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@Service
public @interface BotController {

}
