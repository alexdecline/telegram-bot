package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.container.BotCommand;
import com.telegrambot.bot.core.container.BotControllerMethod;
import com.telegrambot.bot.core.container.dialogwithuser.BotDialogContainer;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;

public class CommandDialogInterceptor extends AbstractSingleHandler {

    private BotDialogContainer botDialogContainer;

    public CommandDialogInterceptor(BotDialogContainer botDialogContainer) {
        this.botDialogContainer = botDialogContainer;
    }

    public CommandDialogInterceptor(SingleHandler nextHandler, BotDialogContainer botDialogContainer) {
        super(nextHandler);
        this.botDialogContainer = botDialogContainer;
    }

    @Override
    public void handle(Update update, BotRequestResponseInfoProvider info) {
        UserDetailInfo userDetails = info.getUserDetailInfo();

        if (userDetails != null && userDetails.getIsDialogActive()) {
            String text = getInputMsgTxt(update);
            userDetails.setLastUSerAnswer(new UserDialogAnswer(text));
            Optional<BotCommand> lastCommand = userDetails.getLastCommand();

            if (lastCommand.isPresent()) {
                BotControllerMethod botControllerMethod = botDialogContainer.get(lastCommand.get());
                info.setBotControllerMethod(botControllerMethod);
            }

            userDetails.createDialogAndAddToHistory(new UserDialogAnswer(text));
        }

        startNextHandler(update, info);
    }

    private String getInputMsgTxt(Update update) {
        if (update.getMessage() != null) {
            return update.getMessage().getText();
        } else {
            return update.getCallbackQuery().getData();
        }
    }
}
