package com.telegrambot.bot.core.container;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Data
@Slf4j
public class BotControllerMethod {
    private Method method;
    private Object instance;
    private Class<?> returnType;
    private Parameter[] parameters;
    private Class<?>[] parametersTypes;
    private Object[] arguments;
    private BotMethodType methodType;
}
