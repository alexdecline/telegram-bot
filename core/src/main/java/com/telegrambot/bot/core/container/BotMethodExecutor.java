package com.telegrambot.bot.core.container;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

@Slf4j
@Component
public class BotMethodExecutor {

    private final BotCommandContainer commandContainer;
    private final HandlerBotMethodParameters handlerBotMethodParameters;

    public BotMethodExecutor(BotCommandContainer commandContainer, HandlerBotMethodParameters handlerBotMethodParameters) {
        this.commandContainer = commandContainer;
        this.handlerBotMethodParameters = handlerBotMethodParameters;
    }

    public BotApiMethod<? extends Serializable> executeMethod(BotControllerMethod method) {

        Optional<Object> object = execute(method);

        if (object.isPresent()) {
            if (object.get() instanceof BotApiMethod) {

                return (BotApiMethod<? extends Serializable>) object.get();
            }

        } else {
            return new SendMessage();
        }

        throw new UnsupportedOperationException("Возвращающий тип метода не определён");
    }

    public Optional<Object> execute(BotControllerMethod botMethod) {

        Method method = botMethod.getMethod();
        method.setAccessible(true);

        try {
            if (botMethod.getArguments() == null || botMethod.getArguments().length == 0) {
                if (isaVoidMethod(botMethod)) {
                    method.invoke(botMethod.getInstance());
                } else {
                    return Optional.ofNullable(method.invoke(botMethod.getInstance()));
                }

            } else {

                return Optional.ofNullable(method.invoke(botMethod.getInstance(), botMethod.getArguments()));
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error(e.getMessage());
        }

        return Optional.empty();
    }

    boolean isaVoidMethod(BotControllerMethod botMethod) {
        return botMethod.getReturnType() == null;
    }
}
