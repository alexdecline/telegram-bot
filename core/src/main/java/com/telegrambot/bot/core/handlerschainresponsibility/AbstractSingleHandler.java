package com.telegrambot.bot.core.handlerschainresponsibility;

import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.Update;

@Data
public abstract class AbstractSingleHandler implements SingleHandler {

    private SingleHandler nextHandler;

    public AbstractSingleHandler() {
    }

    public AbstractSingleHandler(SingleHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    protected void startNextHandler(final Update update, BotRequestResponseInfoProvider info) {
        if (nextHandler != null) {
            nextHandler.handle(update, info);
        }
    }
}
