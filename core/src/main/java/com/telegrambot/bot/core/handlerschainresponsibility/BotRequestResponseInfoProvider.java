package com.telegrambot.bot.core.handlerschainresponsibility;

import com.telegrambot.bot.core.container.BotCommand;
import com.telegrambot.bot.core.container.BotControllerMethod;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import lombok.Data;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;

import java.io.Serializable;
import java.util.Optional;

@Data
public class BotRequestResponseInfoProvider {
    private CallbackQuery callbackQuery;
    private Message message;
    private UserDetailInfo userDetailInfo;
    private BotControllerMethod botControllerMethod;
    private BotCommand botCommand;
    private BotApiMethod<? extends Serializable> botApiMethod;

    public boolean isDialogActive() {
        if (userDetailInfo != null) {
            return userDetailInfo.getIsDialogActive();
        }

        return false;
    }

    public void setDialogActive(boolean isDialogActive) {
        if (userDetailInfo != null) {
            userDetailInfo.setIsDialogActive(isDialogActive);
        }
    }

    public Optional<User> getUser() {
        if (userDetailInfo != null) {
           return Optional.of(userDetailInfo.getUser());
        }

        return Optional.empty();
    }
}
