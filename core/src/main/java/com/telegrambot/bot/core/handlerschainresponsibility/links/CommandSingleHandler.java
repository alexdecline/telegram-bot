package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.container.BotCommand;
import com.telegrambot.bot.core.container.BotCommandHandler;
import com.telegrambot.bot.core.container.BotControllerMethod;
import com.telegrambot.bot.core.exception.CommandNotFoundException;
import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import org.telegram.telegrambots.meta.api.objects.Update;

public class CommandSingleHandler extends AbstractSingleHandler {

    private final BotCommandHandler commandHandler;

    public CommandSingleHandler(SingleHandler nextHandler,
                                BotCommandHandler commandHandler) {
        super(nextHandler);
        this.commandHandler = commandHandler;
    }

    public CommandSingleHandler(BotCommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public void handle(final Update update, BotRequestResponseInfoProvider info) {

        BotControllerMethod methodByCommandString = null;
        if (!info.isDialogActive()) {

            try {
                methodByCommandString = commandHandler.findMethodByCommandString(info.getMessage().getText());
            } catch (CommandNotFoundException e) {
                e.printStackTrace();
            }

            info.getUserDetailInfo().addCommandToHistory(new BotCommand(info.getMessage().getText()));
            info.setBotControllerMethod(methodByCommandString);
        }

        startNextHandler(update, info);
    }
}
