package com.telegrambot.bot.core.container.dialogwithuser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CorrectAnswer {
    private String answer;
}
