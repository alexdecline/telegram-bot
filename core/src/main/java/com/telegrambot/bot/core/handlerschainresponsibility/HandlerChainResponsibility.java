package com.telegrambot.bot.core.handlerschainresponsibility;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;
import java.util.Optional;

public class HandlerChainResponsibility {

    private SingleHandler startHandler;

    void setStartHandler(SingleHandler commandAnswerInterceptor) {
        this.startHandler = commandAnswerInterceptor;
    }

    public Optional<BotApiMethod<? extends Serializable>> getMethod(Update update) {
        BotRequestResponseInfoProvider info = new BotRequestResponseInfoProvider();
        startHandler.handle(update, info);

        return Optional.ofNullable(info.getBotApiMethod());
    }
}
