package com.telegrambot.bot.core.container;

import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

@Component
public class HandlerBotMethodParameters {

    public void resolveParametersAndGetArguments(BotRequestResponseInfoProvider info, Message message) {
        if (info.getBotControllerMethod() == null) {
            return;
        }

        List<Object> arguments = new ArrayList<>();
        BotControllerMethod method = info.getBotControllerMethod();

        for (int i = 0; i < method.getParametersTypes().length; i++) {
            arguments.add(getArgument(method, i, message, info));
        }

        method.setArguments(arguments.toArray());
    }

    private Object getArgument(BotControllerMethod botMethod, int index, Message message,
                               BotRequestResponseInfoProvider info) {
        Class<?> parametersType = botMethod.getParametersTypes()[index];
        Parameter parameter = botMethod.getParameters()[index];
        String[] var1 = parametersType.getName().split("\\.");

        switch (var1[var1.length - 1]) {
            case ("Message"):
                return message;

            case ("Contact"):
                return message.getContact();

            case ("CallbackQuery"):
                return info.getCallbackQuery();

            case ("Long"):
                return identifyByParamName(parameter, message);

            case ("User"):
                return message.getFrom();

            case ("BotRequestResponseInfoProvider"):
                return info;

            case ("UserDetailInfo"):
                return info.getUserDetailInfo();

            case ("UserDialogAnswer"):
                return info.getUserDetailInfo().getLastUSerAnswer();

            case ("CorrectAnswer"):
                return info.getUserDetailInfo().getCorrectAnswer();

        }

        throw new IllegalArgumentException("Argument " + var1[var1.length - 1] + " Not Supported");
    }

    private Object identifyByParamName(Parameter parameter, Message message) {

        switch (parameter.getName().toLowerCase()) {
            case ("chatid"):
                return message.getChatId();
        }

        throw new IllegalArgumentException("Argument Not Supported");
    }
}
