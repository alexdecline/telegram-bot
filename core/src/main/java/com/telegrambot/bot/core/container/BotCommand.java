package com.telegrambot.bot.core.container;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BotCommand {
    private String command;

    public BotCommand(String command) {
        prepareCommand(command);
    }

    private void prepareCommand(String value) {

        if (!value.startsWith("/")) {
            value = "/".concat(value);
        }

        value = value.toLowerCase();
        command = value;
    }
}
