package com.telegrambot.bot.core.container;

public interface BotContainer {
    void addCommand(String command, BotControllerMethod method);
}
