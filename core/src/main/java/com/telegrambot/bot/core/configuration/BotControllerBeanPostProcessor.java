package com.telegrambot.bot.core.configuration;

import com.telegrambot.bot.core.container.BotCommandContainer;
import com.telegrambot.bot.core.container.BotContainer;
import com.telegrambot.bot.core.container.BotControllerMethod;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Component
public class BotControllerBeanPostProcessor implements BeanPostProcessor {

    private final BotContainer commandContainer;
    private final BotContainer dialogMethodContainer;

    public BotControllerBeanPostProcessor(@Qualifier("botCommandContainer") BotCommandContainer commandContainer,
                                          @Qualifier("botDialogContainer") BotContainer dialogMethodContainer) {
        this.commandContainer = commandContainer;
        this.dialogMethodContainer = dialogMethodContainer;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> aClass = bean.getClass();
        Annotation[] annotations = aClass.getAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation instanceof BotController) {
                parseMethods(aClass, bean);
            }
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    private void parseMethods(Class<?> aClass, Object bean) {
        for (Method method : aClass.getDeclaredMethods()) {
            BotCommand annotationCommand = method.getAnnotation(BotCommand.class);
            DialogAfterCommand annotationDialog = method.getAnnotation(DialogAfterCommand.class);

            if (annotationCommand != null) {
                addCommandToContainer(bean, method, getParseValue(annotationCommand.value()), commandContainer);
            } else if (annotationDialog != null) {
                addCommandToContainer(bean, method, getParseValue(annotationDialog.value()), dialogMethodContainer);
            }
        }
    }

    private void addCommandToContainer(Object bean, Method method, String value, BotContainer commandContainer) {
        Parameter[] parameters = method.getParameters();
        Class<?> returnType = method.getReturnType();
        Class<?>[] parametersTypes = method.getParameterTypes();

        BotControllerMethod var = new BotControllerMethod();

        var.setInstance(bean);
        var.setMethod(method);
        var.setParameters(parameters);
        var.setReturnType(returnType);
        var.setParametersTypes(parametersTypes);

        commandContainer.addCommand(value, var);
    }

    private String getParseValue(String value) {
        if (!value.startsWith("/")) {
            value = "/".concat(value);
        }
        value = value.toLowerCase();

        return value;
    }
}
