package com.telegrambot.bot.core.configuration;

import com.telegrambot.bot.core.Bot;
import com.telegrambot.bot.core.container.BotMethodExecutor;
import com.telegrambot.bot.core.handlerschainresponsibility.ChainResponsibilityConfig;
import com.telegrambot.bot.core.handlerschainresponsibility.HandlerChainResponsibility;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.ApiContext;

@Import(ChainResponsibilityConfig.class)
@Configuration

@PropertySource("classpath:core.properties")
public class BotConfiguration {

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;

    private final BotMethodExecutor executor;
    private final HandlerChainResponsibility handlerChainResponsibility;

    public BotConfiguration(BotMethodExecutor executor,
                            @Qualifier("handlerChainResponsibility") HandlerChainResponsibility handlerChainResponsibility) {
        this.executor = executor;
        this.handlerChainResponsibility = handlerChainResponsibility;
    }

    @Bean
    public TelegramLongPollingBot telegramLongPollingBot() {
        DefaultBotOptions options = ApiContext.getInstance(DefaultBotOptions.class);

        return new Bot(options, token, username, executor, handlerChainResponsibility);
    }
}
