package com.telegrambot.bot.core.container;

public enum BotMethodType {
    RUN_ONCE_METHOD,
    METHOD_WITH_ANSWER
}
