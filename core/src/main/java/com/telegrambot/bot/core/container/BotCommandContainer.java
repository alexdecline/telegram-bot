package com.telegrambot.bot.core.container;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@EqualsAndHashCode
@ToString
@Component("botCommandContainer")
public class BotCommandContainer implements BotContainer {

    private Map<BotCommand, BotControllerMethod> commands;

    public BotCommandContainer() {
        commands = new HashMap<>();
    }

    public BotControllerMethod findByCommand(BotCommand command) {
        return this.commands.get(command);
    }

    public void addCommand(String command, BotControllerMethod method) {
        this.commands.put(new BotCommand(command), method);
    }
}