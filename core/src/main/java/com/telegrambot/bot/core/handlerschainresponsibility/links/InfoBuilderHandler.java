package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import com.telegrambot.bot.repository.repository.UserRepository;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import com.telegrambot.bot.service.usersinfoservice.UserService;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

public class InfoBuilderHandler extends AbstractSingleHandler {

    private final UserService userService;

    public InfoBuilderHandler(UserService userService) {
        this.userService = userService;
    }

    public InfoBuilderHandler(SingleHandler nextHandler, UserService userService) {
        super(nextHandler);
        this.userService = userService;
    }

    @Override
    public void handle(Update update, BotRequestResponseInfoProvider info) {

        if (update.getMessage() != null) {
            User user = update.getMessage().getFrom();
            UserDetailInfo userDetails = userService.getUserDetails(user);

            info.setMessage(update.getMessage());
            info.setUserDetailInfo(userDetails);
            userService.updateUserChatId(update.getMessage().getChatId(), userDetails);
        }

        startNextHandler(update, info);
    }
}
