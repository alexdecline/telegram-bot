package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.container.BotControllerMethod;
import com.telegrambot.bot.core.container.BotMethodExecutor;
import com.telegrambot.bot.core.container.dialogwithuser.BotDialogQuestion;
import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.Serializable;

public class BotMethodExecutorInterceptor extends AbstractSingleHandler {

    private final BotMethodExecutor executor;

    public BotMethodExecutorInterceptor(BotMethodExecutor executor) {
        this.executor = executor;
    }

    public BotMethodExecutorInterceptor(SingleHandler nextHandler, BotMethodExecutor executor) {
        super(nextHandler);
        this.executor = executor;
    }

    @Override
    public void handle(Update update, BotRequestResponseInfoProvider info) {
        BotControllerMethod botControllerMethod = info.getBotControllerMethod();
        UserDetailInfo userDetailInfo = info.getUserDetailInfo();
        info.setDialogActive(false);

        BotApiMethod<? extends Serializable> botApiMethod = null;
        if (botControllerMethod != null) {
            botApiMethod = executor.executeMethod(botControllerMethod);
            info.setBotApiMethod(botApiMethod);
        }

        if (info.isDialogActive() && info.getBotApiMethod() != null) {
            if (info.getBotApiMethod() instanceof SendMessage) {
                String text = ((SendMessage) info.getBotApiMethod()).getText();
                userDetailInfo.setLastBotQuestion(new BotDialogQuestion(text));
            }
        }

        startNextHandler(update,info);
    }
}
