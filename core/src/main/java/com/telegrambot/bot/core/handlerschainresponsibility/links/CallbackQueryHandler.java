package com.telegrambot.bot.core.handlerschainresponsibility.links;

import com.telegrambot.bot.core.handlerschainresponsibility.AbstractSingleHandler;
import com.telegrambot.bot.core.handlerschainresponsibility.BotRequestResponseInfoProvider;
import com.telegrambot.bot.core.handlerschainresponsibility.SingleHandler;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import com.telegrambot.bot.service.usersinfoservice.UserService;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

public class CallbackQueryHandler extends AbstractSingleHandler {

    private final UserService userService;

    public CallbackQueryHandler(UserService userService) {
        this.userService = userService;
    }

    public CallbackQueryHandler(SingleHandler nextHandler, UserService userService) {
        super(nextHandler);
        this.userService = userService;
    }

    @Override
    public void handle(Update update, BotRequestResponseInfoProvider info) {

        if (update.getCallbackQuery() != null) {
            info.setCallbackQuery(update.getCallbackQuery());
            User user = update.getCallbackQuery().getFrom();
            info.setMessage(update.getCallbackQuery().getMessage());

            UserDetailInfo userDetails = userService.getUserDetails(user);
            info.setUserDetailInfo(userDetails);
            userService.updateUserChatId(update.getCallbackQuery().getMessage().getChatId(), userDetails);
        }

        startNextHandler(update, info);
    }
}
