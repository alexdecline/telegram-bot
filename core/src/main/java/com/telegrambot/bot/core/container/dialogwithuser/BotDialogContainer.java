package com.telegrambot.bot.core.container.dialogwithuser;

import com.telegrambot.bot.core.container.BotCommand;
import com.telegrambot.bot.core.container.BotContainer;
import com.telegrambot.bot.core.container.BotControllerMethod;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component("botDialogContainer")
public class BotDialogContainer extends ConcurrentHashMap<BotCommand, BotControllerMethod> implements BotContainer {

    @Override
    public void addCommand(String command, BotControllerMethod method) {
        put(new BotCommand(command), method);
    }
}
