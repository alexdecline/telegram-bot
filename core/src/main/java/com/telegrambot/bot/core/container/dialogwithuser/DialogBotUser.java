package com.telegrambot.bot.core.container.dialogwithuser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DialogBotUser {
    private BotDialogQuestion question;
    private UserDialogAnswer answer;
}
