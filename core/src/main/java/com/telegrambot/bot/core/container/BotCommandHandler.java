package com.telegrambot.bot.core.container;

import com.telegrambot.bot.core.exception.CommandNotFoundException;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface BotCommandHandler {

    boolean isHasCommand(Update update);

    BotControllerMethod findMethodByCommand(BotCommand command) throws CommandNotFoundException;

    BotControllerMethod findMethodByCommandString(String command) throws CommandNotFoundException;
}
