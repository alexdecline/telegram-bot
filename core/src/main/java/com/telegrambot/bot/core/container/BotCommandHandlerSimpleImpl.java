package com.telegrambot.bot.core.container;

import com.telegrambot.bot.core.exception.CommandNotFoundException;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class BotCommandHandlerSimpleImpl implements BotCommandHandler {

    private final BotCommandContainer botCommandContainer;

    public BotCommandHandlerSimpleImpl(BotCommandContainer botCommandContainer) {
        this.botCommandContainer = botCommandContainer;
    }

    @Override
    public boolean isHasCommand(Update update) {
        return update.getMessage().getText().startsWith("/");
    }

    @Override
    public BotControllerMethod findMethodByCommand(BotCommand command) throws CommandNotFoundException {
        BotControllerMethod method = botCommandContainer.findByCommand(command);

        if (method == null) {
            throw new CommandNotFoundException(command.getCommand() + "Нет такой комманды");
        }

        return method;
    }

    @Override
    public BotControllerMethod findMethodByCommandString(String command) throws CommandNotFoundException {
        return findMethodByCommand(new BotCommand(command));
    }
}
