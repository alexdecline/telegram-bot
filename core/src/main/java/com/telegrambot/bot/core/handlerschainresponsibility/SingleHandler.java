package com.telegrambot.bot.core.handlerschainresponsibility;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface SingleHandler {
    void handle(final Update update, BotRequestResponseInfoProvider info);
}