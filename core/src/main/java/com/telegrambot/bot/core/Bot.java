package com.telegrambot.bot.core;

import com.telegrambot.bot.core.container.BotMethodExecutor;
import com.telegrambot.bot.core.handlerschainresponsibility.HandlerChainResponsibility;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.Serializable;
import java.util.Optional;

@Slf4j
public class Bot extends TelegramLongPollingBot {
    private final static String ERROR = "ОШИБКА: %s";

    private final String token;
    private final String username;
    private final BotMethodExecutor executor;
    private final HandlerChainResponsibility handlerChainResponsibility;

    public Bot(DefaultBotOptions options,
               String token,
               String username,
               BotMethodExecutor executor,
               HandlerChainResponsibility handlerChainResponsibility
    ) {
        super(options);
        this.token = token;
        this.username = username;
        this.executor = executor;
        this.handlerChainResponsibility = handlerChainResponsibility;
    }

    @Override
    public void onUpdateReceived(Update update) {
        Optional<BotApiMethod<? extends Serializable>> method = handlerChainResponsibility.getMethod(update);
        method.ifPresent(this::executeBotMethod);
    }

    public void executeBotMethod(BotApiMethod<? extends Serializable> method) {

        try {
            Serializable execute = execute(method);
            log.info(execute.toString());
        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }
    }

    private void sendErrorMsg(TelegramApiException e, Long chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(String.format(ERROR, e.getMessage()));

        try {
            execute(sendMessage);
        } catch (TelegramApiException ex) {
            log.error("Can't send ERROR msg");
        }
    }

    @Override
    public String getBotUsername() {
        return this.username;
    }

    @Override
    public String getBotToken() {
        return this.token;
    }
}
