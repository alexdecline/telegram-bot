package com.telegrambot.bot.core.handlerschainresponsibility;

import com.telegrambot.bot.core.container.BotCommandHandler;
import com.telegrambot.bot.core.container.BotMethodExecutor;
import com.telegrambot.bot.core.container.HandlerBotMethodParameters;
import com.telegrambot.bot.core.container.dialogwithuser.BotDialogContainer;
import com.telegrambot.bot.core.handlerschainresponsibility.links.*;
import com.telegrambot.bot.repository.repository.UserRepository;
import com.telegrambot.bot.service.usersinfoservice.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChainResponsibilityConfig {

    private final BotCommandHandler commandHandler;
    private final UserService userService;
    private final BotDialogContainer botDialogContainer;
    private final HandlerBotMethodParameters handlerBotMethodParameters;
    private final BotMethodExecutor botMethodExecutor;
    private final UserRepository userRepository;

    public ChainResponsibilityConfig(BotCommandHandler commandHandler,
                                     UserService userService,
                                     BotDialogContainer botDialogContainer,
                                     HandlerBotMethodParameters handlerBotMethodParameters, BotMethodExecutor botMethodExecutor, UserRepository userRepository) {
        this.commandHandler = commandHandler;
        this.userService = userService;
        this.botDialogContainer = botDialogContainer;
        this.handlerBotMethodParameters = handlerBotMethodParameters;
        this.botMethodExecutor = botMethodExecutor;
        this.userRepository = userRepository;
    }

    @Bean
    public SingleHandler getFirstHandler() {
        return new InfoBuilderHandler(getCallbackQueryHandler(), userService);
    }

    @Bean
    public SingleHandler getCallbackQueryHandler() {
        return new CallbackQueryHandler(getDialogInterceptor(), userService);
    }

    @Bean
    public SingleHandler getDialogInterceptor() {
        return new CommandDialogInterceptor(getCommandSingleHandler(), botDialogContainer);
    }

    @Bean
    public SingleHandler getCommandSingleHandler() {
        return new CommandSingleHandler(getMethodArgumentHandler(), commandHandler);
    }

    @Bean
    public SingleHandler getMethodArgumentHandler() {
        return new MethodArgumentHandler(getBotMethodExecutorInterceptor(), handlerBotMethodParameters);
    }

    @Bean
    public SingleHandler getBotMethodExecutorInterceptor() {
        return new BotMethodExecutorInterceptor(botMethodExecutor);
    }

//    @Bean
//    public SingleHandler getCommandAnswerInterceptor() {
//        return new CommandAnswerInterceptor();
//    }

    @Bean("handlerChainResponsibility")
    public HandlerChainResponsibility getHandlerChainResponsibility() {
        HandlerChainResponsibility hcr = new HandlerChainResponsibility();
        hcr.setStartHandler(getFirstHandler());

        return hcr;
    }
}
