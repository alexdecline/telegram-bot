package com.telegrambot.bot.controller;

import com.telegrambot.bot.core.configuration.BotCommand;
import com.telegrambot.bot.core.configuration.BotController;
import com.telegrambot.bot.core.configuration.DialogAfterCommand;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.repository.entity.DaoUser;
import com.telegrambot.bot.repository.entity.UserRole;
import com.telegrambot.bot.repository.entity.enums.Role;
import com.telegrambot.bot.repository.repository.UserRepository;
import com.telegrambot.bot.repository.repository.UserRoleRepository;
import com.telegrambot.bot.service.menu.mainmenu.MainMenuService;
import com.telegrambot.bot.service.menu.testmenu.TestsMenuService;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.io.Serializable;

@Slf4j
@BotController
public class BaseController {
    private final MainMenuService mainMenuService;
    private final TestsMenuService testsMenuService;

    public BaseController(@Qualifier("mainMenuServiceImpl") MainMenuService mainMenuService,
                          TestsMenuService testsMenuService) {
        this.mainMenuService = mainMenuService;
        this.testsMenuService = testsMenuService;
    }

    @BotCommand(value = "Tests")
    public BotApiMethod<? extends Serializable> someMethod(Long chatId) {
        ReplyKeyboard menu = testsMenuService.getMenu();
        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(chatId);
        sendMessage.setReplyMarkup(menu);
        sendMessage.setText("Выберете тест");

        return sendMessage;
    }

    @BotCommand(value = "Main menu")
    public SendMessage getMainMenu(Long chatId) {
        ReplyKeyboard menu = mainMenuService.getMenu();
        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(chatId);
        sendMessage.setReplyMarkup(menu);
        sendMessage.setText("Выберете раздел");

        return sendMessage;
    }

    @BotCommand(value = "start")
    public SendMessage getMainMenuStart(Long chatId) {
        ReplyKeyboard menu = mainMenuService.getMenu();
        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(chatId);
        sendMessage.setReplyMarkup(menu);
        sendMessage.setText("Выберете раздел");

        return sendMessage;
    }
}