package com.telegrambot.bot.controller;

import com.telegrambot.bot.core.configuration.BotCommand;
import com.telegrambot.bot.core.configuration.BotController;
import com.telegrambot.bot.core.configuration.DialogAfterCommand;
import com.telegrambot.bot.core.container.dialogwithuser.CorrectAnswer;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.service.game.game1.GameOne;
import com.telegrambot.bot.service.game.game2.GameTwo;
import com.telegrambot.bot.service.menu.gememenu.GameMenuService;
import com.telegrambot.bot.service.menu.mainmenu.MainMenuService;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

import java.io.Serializable;
import java.util.Optional;

@Slf4j
@BotController
public class GamesController {
    private final GameMenuService gameMenuService;
    private final MainMenuService mainMenuService;
    private final GameOne gameOne;
    private final GameTwo gameTwo;

    public GamesController(GameMenuService gameMenuService,
                           GameOne gameOne,
                           @Qualifier("mainMenuServiceImpl") MainMenuService mainMenuService,
                           GameTwo gameTwo) {
        this.gameMenuService = gameMenuService;
        this.gameOne = gameOne;
        this.mainMenuService = mainMenuService;
        this.gameTwo = gameTwo;
    }

    @BotCommand("Games")
    public SendMessage getMGameMenu(Long chatId) {
        SendMessage sendMessage = new SendMessage();
        ReplyKeyboard menu = gameMenuService.getMenu();

        sendMessage.setReplyMarkup(menu);
        sendMessage.setText("Выбирай игру");
        sendMessage.setChatId(chatId);

        return sendMessage;
    }

    @BotCommand("Game 1")
    public BotApiMethod<? extends Serializable> gameOne(Long chatId,
                                                        UserDetailInfo info) {
        info.setIsDialogActive(true);
        Optional<SendMessage> play = gameOne.play(info);

        SendMessage sendMessage = null;
        if (play.isPresent()) {
            sendMessage = play.get();
            sendMessage.setChatId(chatId);
        }

        return sendMessage;
    }

    @DialogAfterCommand("Game 1")
    public BotApiMethod<? extends Serializable> afterCommandTestMethod(UserDialogAnswer lastUserAnswer,
                                                                       CorrectAnswer correctAnswer,
                                                                       Long chatId) {
        boolean isAnswerCorrect = gameOne.isAnswerCorrect(lastUserAnswer, correctAnswer);
        SendMessage sendMessage = gameMenuService.afterWinMenu(isAnswerCorrect, chatId);
        ReplyKeyboard menu = mainMenuService.getMenu();
        sendMessage.setReplyMarkup(menu);

        return sendMessage;
    }

    @BotCommand("Game 2")
    public BotApiMethod<? extends Serializable> gameTwo(Long chatId, UserDetailInfo info) {
        info.setIsDialogActive(true);
        Optional<SendMessage> play = gameTwo.play(info);
        SendMessage sendMessage = null;
        if (play.isPresent()) {
            sendMessage = play.get();
            sendMessage.setChatId(chatId);
        }

        return sendMessage;
    }

    @DialogAfterCommand("Game 2")
    public BotApiMethod<? extends Serializable> afterCommandGameTwo(UserDialogAnswer lastUserAnswer,
                                                                       CorrectAnswer correctAnswer,
                                                                       Long chatId) {

        SendMessage sendMessage = gameMenuService.afterWinMenu(false, chatId);
        ReplyKeyboard menu = mainMenuService.getMenu();
        sendMessage.setReplyMarkup(menu);

        return sendMessage;
    }
}
