package com.telegrambot.bot.controller;

import com.telegrambot.bot.core.configuration.BotCommand;
import com.telegrambot.bot.core.configuration.BotController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendInvoice;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.payments.LabeledPrice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@BotController
@Component
public class TestsController {

    @BotCommand("TestsPayment")
    public BotApiMethod<? extends Serializable> getMGameMenu(Long chatId) {
        SendInvoice sendInvoice = new SendInvoice();
        List<LabeledPrice> labeledPrices = new ArrayList<>();
        LabeledPrice labeledPrice = new LabeledPrice();
        labeledPrices.add(labeledPrice);

        labeledPrice.setLabel("Test Label price");
        labeledPrice.setAmount(40000);

        sendInvoice.setChatId(Integer.parseInt(chatId.toString()));
        sendInvoice.setTitle("Test Invoice");
        sendInvoice.setDescription("Test Description");
        sendInvoice.setPayload("111");
        sendInvoice.setProviderToken("410694247:TEST:74fcee53-7b55-4ab9-ba70-e70e55dd98ac");
        sendInvoice.setProviderToken("410694247:TEST:74fcee53-7b55-4ab9-ba70-e70e55dd98ac");
        sendInvoice.setStartParameter("test");
        sendInvoice.setCurrency("RUB");
        sendInvoice.setPrices(labeledPrices);
        sendInvoice.setFlexible(true);


        return sendInvoice;
    }

    @BotCommand(value = "UserInfo")
    public BotApiMethod<? extends Serializable> getUserInfo(Message message) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Your contact Inf: " + message.getFrom());
        sendMessage.setChatId(message.getChatId());

        return sendMessage;
    }

}
