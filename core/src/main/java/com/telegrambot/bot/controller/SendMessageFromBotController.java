package com.telegrambot.bot.controller;

import com.telegrambot.bot.core.configuration.BotCommand;
import com.telegrambot.bot.core.configuration.BotController;
import com.telegrambot.bot.core.configuration.DialogAfterCommand;
import com.telegrambot.bot.repository.entity.DaoUser;
import com.telegrambot.bot.repository.repository.UserRepository;
import com.telegrambot.bot.service.menu.mainmenu.MainMenuService;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Slf4j
@BotController
@Component
public class SendMessageFromBotController {
    private final UserRepository userRepository;
    private final HashMap<User, DaoUser> usersSenderContainer = new HashMap<>();
    private final MainMenuService mainMenuService;


    public SendMessageFromBotController(UserRepository userRepository, @Qualifier("mainMenuServiceImpl") MainMenuService mainMenuService) {
        this.userRepository = userRepository;
        this.mainMenuService = mainMenuService;
    }

    @BotCommand("Send message")
    public BotApiMethod<? extends Serializable> chooseUser(Long chatId,
                                                           UserDetailInfo info) {

        info.setIsDialogActive(true);

        List<DaoUser> all = userRepository.findAll();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboardRowListList = new ArrayList<>();

        sendMessage.setText("Выберите пользователя");

        if (chatId.equals(153277235L)) {
            for (DaoUser daoUser : all) {
                InlineKeyboardButton keyboardRow = new InlineKeyboardButton();
                List<InlineKeyboardButton> keyboardRowList = new ArrayList<>();

                keyboardRow.setText(daoUser.getFirstName());
                keyboardRow.setCallbackData(daoUser.getTelegramId().toString());
                keyboardRowList.add(keyboardRow);
                keyboardRowListList.add(keyboardRowList);
            }
        } else {
            InlineKeyboardButton keyboardRow = new InlineKeyboardButton();
            List<InlineKeyboardButton> keyboardRowList = new ArrayList<>();

            keyboardRow.setText("Alex");
            keyboardRow.setCallbackData("153277235");
            keyboardRowList.add(keyboardRow);
            keyboardRowListList.add(keyboardRowList);
        }

        markup.setKeyboard(keyboardRowListList);
        sendMessage.setReplyMarkup(markup);

        return sendMessage;
    }


    @DialogAfterCommand("Send message")
    public BotApiMethod<? extends Serializable> sendMessageUsers(UserDetailInfo userDetailInfo, Long chatId) {
        SendMessage sendMessage = new SendMessage();

        if (usersSenderContainer.get(userDetailInfo.getUser()) == null) {
            Optional<DaoUser> daoUser = userRepository.findByTelegramId(Integer.parseInt(userDetailInfo.getLastUSerAnswer().getAnswer()));
            sendMessage.setChatId(chatId);

            if (daoUser.isPresent()) {
                usersSenderContainer.put(userDetailInfo.getUser(), daoUser.get());
                userDetailInfo.setIsDialogActive(true);
                sendMessage.setText(String.format("Напишите сообщение пользователю %S", daoUser.get().getFirstName()));
            } else {
                sendMessage.setText("Пользователь не найден");
            }

        } else {
            sendMessage.setText(userDetailInfo.getLastUSerAnswer().getAnswer());
            DaoUser daoUser = usersSenderContainer.get(userDetailInfo.getUser());
            usersSenderContainer.remove(userDetailInfo.getUser());
            ReplyKeyboard menu = mainMenuService.getMenu();
            sendMessage.setReplyMarkup(menu);
            sendMessage.setChatId(daoUser.getChatId());
        }

        return sendMessage;
    }
}
