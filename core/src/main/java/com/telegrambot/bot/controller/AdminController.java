package com.telegrambot.bot.controller;

import com.telegrambot.bot.core.configuration.BotCommand;
import com.telegrambot.bot.core.configuration.BotController;
import com.telegrambot.bot.core.configuration.DialogAfterCommand;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.repository.entity.DaoUser;
import com.telegrambot.bot.repository.entity.UserRole;
import com.telegrambot.bot.repository.entity.enums.Role;
import com.telegrambot.bot.repository.repository.UserRepository;
import com.telegrambot.bot.repository.repository.UserRoleRepository;
import com.telegrambot.bot.service.menu.mainmenu.MainMenuService;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

@BotController
public class AdminController {
    private final UserRepository userRepository;
    private final MainMenuService mainMenuService;
    private final UserRoleRepository userRoleRepository;

    public AdminController(UserRepository userRepository,
                           @Qualifier("adminMenu") MainMenuService mainMenuService,
                           UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.mainMenuService = mainMenuService;
        this.userRoleRepository = userRoleRepository;
    }

    @BotCommand(value = "addRole")
    public SendMessage addRole(Long chatId, UserDetailInfo info) {
        SendMessage sendMessage = new SendMessage();
        info.setIsDialogActive(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText("Выберете роль");

        return sendMessage;
    }

    @DialogAfterCommand("addRole")
    public SendMessage addRole(UserDialogAnswer lastUserAnswer, Long chatId, UserDetailInfo info) {
        SendMessage sendMessage = new SendMessage();
        ReplyKeyboard menu = mainMenuService.getMenu();
        sendMessage.setReplyMarkup(menu);
        sendMessage.setChatId(chatId);
        DaoUser daoUser = info.getDaoUser();

        UserRole userRole = new UserRole();
        userRole.setUser(daoUser);
        userRole.setRole(Role.valueOf(lastUserAnswer.getAnswer()));
        UserRole role = userRoleRepository.save(userRole);

        daoUser.addRole(role);
        userRepository.save(daoUser);

        return sendMessage;
    }
}
