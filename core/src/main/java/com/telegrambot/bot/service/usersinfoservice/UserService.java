package com.telegrambot.bot.service.usersinfoservice;

import org.telegram.telegrambots.meta.api.objects.User;

public interface UserService {

    UserDetailInfo getUserDetails(User user);

    void addUserInfo(User user, UserDetailInfo userDetailInfo);

    boolean isUserExist(User user);

    void updateUserChatId(Long chatId, UserDetailInfo userDetails);
}
