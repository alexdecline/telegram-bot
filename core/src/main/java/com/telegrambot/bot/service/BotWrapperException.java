package com.telegrambot.bot.service;

public class BotWrapperException extends RuntimeException {

    public BotWrapperException(String message) {
        super(message);
    }

    public BotWrapperException(String message, Throwable cause) {
        super(message, cause);
    }

    public BotWrapperException(Throwable cause) {
        super(cause);
    }
}
