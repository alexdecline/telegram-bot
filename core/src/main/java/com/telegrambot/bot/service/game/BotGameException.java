package com.telegrambot.bot.service.game;

public class BotGameException extends RuntimeException {
    public BotGameException(String message) {
        super(message);
    }

    public BotGameException(String message, Throwable cause) {
        super(message, cause);
    }

    public BotGameException(Throwable cause) {
        super(cause);
    }
}
