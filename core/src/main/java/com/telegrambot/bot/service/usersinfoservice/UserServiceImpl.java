package com.telegrambot.bot.service.usersinfoservice;

import com.telegrambot.bot.repository.entity.DaoUser;
import com.telegrambot.bot.repository.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private UsersContainer usersContainer = new UsersContainer();

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDetailInfo getUserDetails(User user) {
        UserDetailInfo userDetailInfo = usersContainer.get(user);

        if (userDetailInfo == null) {
            DaoUser daoUser = findOrCreateDaoUser(user);
            userDetailInfo = new UserDetailInfo();
            userDetailInfo.setUser(user);
            userDetailInfo.setDaoUser(daoUser);

            usersContainer.put(user, userDetailInfo);
        }

        return userDetailInfo;
    }

    public void addUserInfo(User user, UserDetailInfo userDetailInfo) {
        usersContainer.put(user, userDetailInfo);
    }

    public boolean isUserExist(User user) {
        return usersContainer.containsKey(user);
    }

    @Override
    public void updateUserChatId(Long chatId, UserDetailInfo userDetails) {
        DaoUser daoUser = userDetails.getDaoUser();
        if (!chatId.equals(daoUser.getChatId())) {
            daoUser.setChatId(chatId);
            userRepository.save(daoUser);
        }
    }

    public DaoUser findOrCreateDaoUser(User user) {
        Optional<DaoUser> daoUser = userRepository.findByTelegramId(user.getId());

        if (!daoUser.isPresent()) {
            DaoUser result = new DaoUser();
            result.setTelegramId(user.getId());
            result.setUserName(user.getUserName());
            result.setFirstName(user.getFirstName());
            result.setLastName(user.getLastName());
            return userRepository.save(result);

        } else {
            return daoUser.get();
        }
    }
}
