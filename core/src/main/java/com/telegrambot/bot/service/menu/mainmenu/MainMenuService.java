package com.telegrambot.bot.service.menu.mainmenu;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

public interface MainMenuService {
    ReplyKeyboard getMenu();
}
