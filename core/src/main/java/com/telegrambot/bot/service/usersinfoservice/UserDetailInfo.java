package com.telegrambot.bot.service.usersinfoservice;

import com.telegrambot.bot.core.container.BotCommand;
import com.telegrambot.bot.core.container.dialogwithuser.BotDialogQuestion;
import com.telegrambot.bot.core.container.dialogwithuser.CorrectAnswer;
import com.telegrambot.bot.core.container.dialogwithuser.DialogBotUser;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.repository.entity.DaoUser;
import com.telegrambot.bot.service.game.Game;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@EqualsAndHashCode(exclude = {"lastGame", "commandHistory", "dialogHistory"})
@Data
public class UserDetailInfo {
    private User user;
    private DaoUser daoUser;
    private Game lastGame;
    private BotDialogQuestion lastBotQuestion;
    private UserDialogAnswer lastUSerAnswer;
    private CorrectAnswer correctAnswer;
    private List<BotCommand> commandHistory = new ArrayList<>();
    private List<DialogBotUser> dialogHistory = new ArrayList<>();
    private Boolean isDialogActive = false;

    public void addCommandToHistory(BotCommand command) {
        this.commandHistory.add(command);
    }

    public void addDialogToHistory (DialogBotUser dialogBotUser) {
        this.lastBotQuestion = null;
        this.dialogHistory.add(dialogBotUser);
    }

    public void createDialogAndAddToHistory(UserDialogAnswer answer) {

        if (lastBotQuestion == null) {
            lastBotQuestion = new BotDialogQuestion("");
        }

        this.dialogHistory.add(new DialogBotUser(lastBotQuestion, answer));
        lastBotQuestion = null;
    }

    public void closeDialogSession() {
        isDialogActive = false;
        lastBotQuestion = null;
        dialogHistory = new ArrayList<>();
    }

    public Optional<BotCommand> getLastCommand() {

        if (!commandHistory.isEmpty()) {
            return Optional.of(commandHistory.get(commandHistory.size() - 1));
        }

        return Optional.empty();
    }
}
