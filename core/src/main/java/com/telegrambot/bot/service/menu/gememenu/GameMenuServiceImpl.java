package com.telegrambot.bot.service.menu.gememenu;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameMenuServiceImpl implements GameMenuService{

    @Override
    public ReplyKeyboard getMenu() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardRow keyboardRowTwo = new KeyboardRow();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        keyboardRow.add(0, "Game 1");
        keyboardRow.add(1, "Game 2");

        keyboardRowTwo.add(0, "Main menu");

        keyboardRowList.add(keyboardRow);
        keyboardRowList.add(keyboardRowTwo);
        keyboardMarkup.setKeyboard(keyboardRowList);

        return keyboardMarkup;
    }

    @Override
    public SendMessage afterWinMenu(boolean isWin, Long chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyboardRow = new KeyboardRow();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        sendMessage.setText("You win");

        if (!isWin){
            KeyboardRow replay = new KeyboardRow();
            replay.add("Ещё раз");
            keyboardRowList.add(replay);
            sendMessage.setText("You lose, good day sir");
        }

        keyboardRow.add(0, "Game 1");
        keyboardRow.add(1, "Game 2");


        keyboardRowList.add(keyboardRow);
        keyboardMarkup.setKeyboard(keyboardRowList);

        return sendMessage;
    }
}
