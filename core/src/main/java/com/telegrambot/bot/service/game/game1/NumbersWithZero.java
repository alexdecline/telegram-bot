package com.telegrambot.bot.service.game.game1;

import com.telegrambot.bot.service.BotWrapperException;
import lombok.Getter;

@Getter
public enum NumbersWithZero {

    ZERO(0, 1),
    ONE(1, 0),
    TWO(2, 0),
    THREE(3, 0),
    FOUR(4, 0),
    FIVE(5, 0),
    SIX(6, 1),
    SEVEN(7, 0),
    EIGHT(8, 2),
    NINE(9, 1);

    private int value;
    private int zeros;

    NumbersWithZero(int value, int zeros) {
        this.value = value;
        this.zeros = zeros;
    }

    public static int getSum(String answer) {
        try {
            return answer
                    .chars()
                    .mapToObj(s -> (char) s)
                    .map(c -> Integer.parseInt(String.valueOf(c)))
                    .map(NumbersWithZero::getZerosByValue)
                    .reduce(Integer::sum).orElse(0);

        } catch (NumberFormatException ex) {
            throw new BotWrapperException("Нужно вводить цифры", ex);
        }
    }

    public static int getZerosByValue(Integer value) {

        for (NumbersWithZero numbersWithZero : NumbersWithZero.values()) {
            if (numbersWithZero.value == value) {
                return numbersWithZero.zeros;
            }
        }

        throw new BotWrapperException("Ошибка в получении ENUM");
    }
}
