package com.telegrambot.bot.service.game;

import com.telegrambot.bot.core.container.BotCommand;
import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.User;

@Data
public class GameInfo {
    private User currentUser;
    private BotCommand prevGameCommand;
    private Object correctAnswer;
}
