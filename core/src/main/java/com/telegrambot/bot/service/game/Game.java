package com.telegrambot.bot.service.game;

import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;

import java.io.Serializable;
import java.util.Optional;

public interface Game {
    Optional<? extends BotApiMethod<? extends Serializable>> play(UserDetailInfo info);

    default Optional<BotApiMethod<? extends Serializable>> continueGame(UserDetailInfo info){
        return Optional.empty();
    }
}
