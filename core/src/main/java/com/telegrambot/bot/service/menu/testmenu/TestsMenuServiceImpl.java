package com.telegrambot.bot.service.menu.testmenu;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestsMenuServiceImpl implements TestsMenuService {

    @Override
    public ReplyKeyboard getMenu() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardRow keyboardRowTwo = new KeyboardRow();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        keyboardRow.add(0, "UserInfo");
        keyboardRow.add(1, "TestsPayment");
        keyboardRow.add(2, "Send message");

        keyboardRowTwo.add(0, "Main menu");

        keyboardRowList.add(keyboardRow);
        keyboardRowList.add(keyboardRowTwo);
        keyboardMarkup.setKeyboard(keyboardRowList);

        return keyboardMarkup;
    }
}
