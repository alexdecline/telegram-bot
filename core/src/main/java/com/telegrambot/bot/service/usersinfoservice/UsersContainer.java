package com.telegrambot.bot.service.usersinfoservice;

import com.telegrambot.bot.core.container.BotCommand;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class UsersContainer extends ConcurrentHashMap<User, UserDetailInfo> {

    public void addCommandToHistory(User user, BotCommand command) {
        UserDetailInfo userDetailInfo = get(user);

        if (userDetailInfo == null) {
            userDetailInfo = new UserDetailInfo();
            put(user, userDetailInfo);
        }

        userDetailInfo.addCommandToHistory(command);
    }
}