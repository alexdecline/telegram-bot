package com.telegrambot.bot.service.menu.gememenu;

import com.telegrambot.bot.service.menu.mainmenu.MainMenuService;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface GameMenuService extends MainMenuService {
    SendMessage afterWinMenu(boolean isWin, Long ChatId);
}
