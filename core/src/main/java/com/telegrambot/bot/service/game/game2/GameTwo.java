package com.telegrambot.bot.service.game.game2;

import com.telegrambot.bot.core.container.dialogwithuser.CorrectAnswer;
import com.telegrambot.bot.core.container.dialogwithuser.UserDialogAnswer;
import com.telegrambot.bot.service.game.Game;
import com.telegrambot.bot.service.usersinfoservice.UserDetailInfo;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;

import java.io.Serializable;
import java.util.Optional;
import java.util.Random;

@Service
public class GameTwo implements Game {

    @Override
    public Optional<SendMessage> play(UserDetailInfo info) {
        SendMessage message = new SendMessage();
        ReplyKeyboardRemove keyboardMarkup = new ReplyKeyboardRemove();

        message.setReplyMarkup(keyboardMarkup);
        StringBuilder sb = new StringBuilder();
        sb.append("А теперь сложнее =)").append("\n");
        Random rn = new Random();

        for (int i = 0; i < 4; i++) {
            String value = getValue(rn);
            int sumZeros = rn.nextInt(13);

            if (i == 3) {
                info.setCorrectAnswer(new CorrectAnswer("99999"));
                sb.append(value).append(" = ").append("?").append("\n");
            } else {
                sb.append(value).append(" = ").append(sumZeros).append("\n");
            }
        }

        message.setText(sb.toString());

        return Optional.of(message);
    }


    private String getValue(Random rn) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            result.append(rn.nextInt(10));
        }

        return result.toString();
    }

    public boolean isAnswerCorrect(UserDialogAnswer lastUserAnswer,
                                   CorrectAnswer correctAnswer) {
        return lastUserAnswer.getAnswer().equals(correctAnswer.getAnswer());
    }

    @Override
    public Optional<BotApiMethod<? extends Serializable>> continueGame(UserDetailInfo info) {
        return Optional.empty();
    }
}
