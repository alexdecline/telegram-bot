package com.telegrambot.bot.service.menu.mainmenu;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service("adminMenu")
public class AdminMenu implements MainMenuService {
    @Override
    public ReplyKeyboard getMenu() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardRow keyboardRow = new KeyboardRow();
        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        keyboardRow.add(0, "Games");
        keyboardRow.add(1, "Tests");
        keyboardRow.add(2, "AddRole");

        keyboardRowList.add(keyboardRow);
        keyboardMarkup.setKeyboard(keyboardRowList);

        return keyboardMarkup;
    }
}
